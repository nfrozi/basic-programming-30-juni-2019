<?php
/**
 * Ini adalah file utama aplikasi (entry point)
 * 
 * Changelog:
 * <ul>
 * v1.0.1 > Tambahan x
 * </ul>
 */

include_once 'koneksi_db.php';
include_once 'Penduduk.php';

/**
 * Fungsi untuk menghitung luas segitiga
 * 
 * @author Nanang F. Rozi
 * @since 2019-06-30
 * @version 1.0.1
 * 
 * @param $alas float segitiga
 * @param $tinggi float Tinggi segitiga
 */
function hitungLuasSegitiga($alas, $tinggi)
{
    return $alas * $tinggi;
}

// READ DATA
$penduduk = new Penduduk();

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Uji Sertifikasi</title>
	</head>
	<body>
		<h1>Data Penduduk</h1>
		<a href="form_penduduk.php">Tambah Data</a>
		<table border="1" style="width: 100%">
			<tr>
				<th>KTP</th>
				<th>Nama</th>
				<th>No. HP</th>
				<th></th>
			</tr>
			<?php
			$dataPenduduk = $penduduk->dapatkanData();
			 
            foreach ($dataPenduduk as $barisData) {
                ?>
                <tr>
                    <td><?php echo $barisData['id_penduduk'] ?></td>
                    <td><?php echo $barisData['nama_penduduk'] ?></td>
                    <td><?php echo $barisData['no_hp'] ?></td>
                    <td>
                    	<a href="form_penduduk_edit.php?act=update&id_penduduk=<?php echo $barisData['id_penduduk'] ?>">Edit</a> |
                    	<a href="proses_penduduk.php?act=hapus&id_penduduk=<?php echo $barisData['id_penduduk'] ?>">Hapus</a>
                    </td>
                </tr>
                <?php
            }
			?>
		</table>
	</body>
</html>