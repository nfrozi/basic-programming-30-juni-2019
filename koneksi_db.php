<?php
/**
 * File ini digunakan untuk koneksi ke basis data MySQL
 */

include_once 'konfigurasi.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ( ! $conn) {
    die("Connection failed: " . mysqli_connect_error());
}