<?php
/**
 * Class untuk proses bisnis terkait Penduduk
 * 
 * <strong>Data:</strong>
 * <ul>
 *  <li>KTP</li>
 *  <li>Nama</li>
 *  <li>No. HP</li>
 * </ul>
 * 
 * @author Nanang F. Rozi
 * @author Andy
 *
 * @since 2019-06-30
 * @version 1.0.0
 * 
 * @property $id
 */
class Penduduk {
    
    /**
     * Fungsi untuk mendapatkan data penduduk, urut berdasarkan nama
     * 
     * @param string|null $id KTP
     * @return array|array[]
     */
    function dapatkanData($id = null)
    {
        $time1 = time();
        global $conn;
   
        $data = [];

        $filter = "";
        if ( ! is_null($id)) {
            $filter = " WHERE id_penduduk = '$id'";
        }
        
        
        $sql = "SELECT id_penduduk, nama_penduduk, no_hp
                FROM penduduk
                $filter
                ORDER BY nama_penduduk";
        
        $result = $conn->query($sql);
        
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }
        
        if ( ! is_null($id)) {
            return $data[0];
        }
        
        $time2 = time();
        
        echo '<br>Time = '.($time2-$time1).'<br>';
        
        return $data;
    }
    
    /**
     * Menyimpan data ke DB
     * 
     * @param string $aksi Mungkin "tambah" / "update"
     * @param string $id KTP
     * @param string $nama Nama pend
     * @param string|null $noHp No HP (boleh kosong)
     * 
     * @return mysqli_result|boolean
     */
    function simpanData($aksi, $id, $nama, $noHp = null)
    {
        if ($aksi == 'tambah') {
            return $this->insertData($id, $nama, $noHp);
        }
        elseif ($aksi == 'update') {
            return $this->updateData($id, $nama, $noHp);
        }
    }
    
    /**
     * Insert ke DB
     * 
     * @param string $id KTP
     * @param string $nama Nama pend
     * @param string|null $noHp No HP (boleh kosong)
     * @return mysqli_result|boolean
     */
    function insertData($id, $nama, $noHp)
    {
        global $conn;
        $sql = 
            "INSERT INTO `penduduk`(`id_penduduk`, `nama_penduduk`, `no_hp`) 
             VALUES ('$id', '$nama', '$noHp')";
        
        return $conn->query($sql);
    }
    
    /**
     * Update ke DB
     * 
     * @param string $id KTP
     * @param string $nama Nama pend
     * @param string|null $noHp No HP (boleh kosong)
     * @return mysqli_result|boolean
     */
    function updateData($id, $nama, $noHp)
    {
        global $conn;
        
        $sql =
            "UPDATE `penduduk` 
                SET `nama_penduduk`= '$nama',
                    `no_hp`= '$noHp'
                WHERE id_penduduk = '$id'";
        
        return $conn->query($sql);
    }
    
    /**
     * Hapus data berdasarkan ID
     * 
     * @param string $id
     * @return mysqli_result|boolean
     */
    function hapusData($id)
    {
        global $conn;
        
        $sql = "DELETE FROM `penduduk` WHERE id_penduduk = '$id'";
        
        return $conn->query($sql);
    }
}