<?php

/**
 * File ini untuk memproses data penduduk
 */

include_once 'koneksi_db.php';
include_once 'Penduduk.php';

$penduduk = new Penduduk();

// Didapatkan dari URL
$aksi = $_GET['act'];

switch ($aksi) {
    // proses penambahan data
    case 'tambah':
        // Mengambil data dari form HTML
        $idPenduduk = $_POST['id_penduduk'];
        $namaPenduduk = $_POST['nama_penduduk'];
        $noHp = $_POST['no_hp'];
        
        // Validasi data KTP
        if (strlen($idPenduduk) < 16) {
            echo 'Error: KTP harus 16 karakter';
            die();
        }
        
        $statusSimpan = 
            $penduduk->simpanData($aksi, $idPenduduk, $namaPenduduk, $noHp);
        
        if ($statusSimpan == true) {
            header("Location: index.php");
        }
        else {
            echo 'Error: Gagal menyimpan data.';
        }
        
        break;
        
    case 'hapus':
        $id = $_GET['id_penduduk'];  // dari URL
        $statusHapus = $penduduk->hapusData($id);
        
        if ($statusHapus === true) {
            echo 'Berhasil menghapus data';
            echo '<p><a href="index.php">Kembali</a></p>';
        }
        else {
            echo 'Error: Gagal menyimpan data.';
        }
        
        break;
        
    case 'update':
        
        echo '<pre>';
        print_r($_POST);
        echo '</pre>';
        // Mengambil data dari form HTML
        $idPenduduk = $_POST['id_penduduk'];
        $namaPenduduk = $_POST['nama_penduduk'];
        $noHp = $_POST['no_hp'];
        
        $statusSimpan =
            $penduduk->simpanData($aksi, $idPenduduk, $namaPenduduk, $noHp);
        
        if ($statusSimpan == true) {
            header("Location: index.php");
        }
        else {
            echo 'Error: Gagal menyimpan data.';
        }
        
        break;
}



