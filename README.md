Contoh program yang dibuat saat pelatihan sederhana uji kompetensi Basic Programming di Lab Bahasa Pemrograman, Jurusan Teknik Informatika, Institut Teknologi Adhi Tama Surabaya pada Ahad, 30 Juni 2019.

Program ini menggambarkan secara sederhana unit-unit yang diperkirakan akan diujikan saat uji kompetensi.

Basis data yang digunakan bisa diimpor dari file **db_sertifikasi.sql**