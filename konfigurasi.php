<?php
/**
 * File ini berisi tentang konfigurasi aplikasi
 */

/** 
 * @var string $servername Nama server yang digunakan
 */
$servername = "localhost";

/**
 * @var string $username Username utk konek ke DB
 */
$username = "nfrozi";

/**
 * @var string $password Password untuk konek ke DB
 */
$password = "nfrozi";

/**
 * @var string $dbname Nama Database
 */
$dbname = "db_sertifikasi";