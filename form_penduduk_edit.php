<!-- File ini berisi form untuk edit penduduk  -->

<?php
include_once 'koneksi_db.php';
include_once 'Penduduk.php';

$penduduk = new Penduduk();
$data = $penduduk->dapatkanData($_GET['id_penduduk']);
?>
<form action="proses_penduduk.php?act=update" method="post">
	<p>
		<label>KTP: </label>
		<input name="id_penduduk" value="<?php echo $_GET['id_penduduk'] ?>" 
		       autofocus maxlength="16" required readonly="readonly">
	</p>
	<p>
		<label>Nama: </label>
		<input name="nama_penduduk" 
			value="<?php echo $data['nama_penduduk'] ?>">
	</p>
	<p>
		<label>No. HP: </label>
		<input name="no_hp" 
			value="<?php echo $data['no_hp'] ?>">
	</p>
	<button>Simpan</button>
</form>